# Simple Slack Deleter

Simple Slack Deleter is a short Python script that deletes all messages older
than 6 weeks in a Slack instance. Learn more about
[why you may want to delete older Slack messages](https://puzzling.org/technology/software/2020/05/delete-your-free-slack-backlogs/).

## Installing

1. install [slack_cleaner2](https://github.com/sgratzl/slack_cleaner2): `pip3
   install slack_cleaner2`
2. download the script: `git clone
   https://gitlab.com/puzzlement/simple-slack-deleter`

You will then need to obtain an API key for your Slack instance (see
**Requirements**), and edit the `SECRET_KEY` line in the
`simple-slack-deleter/simple_deleter.py` to contain your API key.

Consider changing `AGE_TO_DELETE` if you'd like to preserve messages for longer
than 6 weeks, or alternatively delete them sooner than that.

## Running

```
cd simple-slack-deleter
python3 simple_deleter.py
```

## Requirements

 * [Owner or Administrator](https://slack.com/intl/en-au/help/articles/201912948-Owners-and-Administrators)
   access to your Slack instance (or else you cannot delete messages other users
   wrote)
 * Python 3
 * [slack_cleaner2](https://github.com/sgratzl/slack_cleaner2)
 * [Slack API key for your workspace](https://github.com/sgratzl/slack_cleaner2#token), for
   [all the scopes listed](https://github.com/sgratzl/slack_cleaner2#user-token-scopes-by-use-case)
   by slack_cleaner2.

## About

### Credits

Simple Slack Deleter was developed by [Mary Gardiner](https://mary.gardiner.id.au/).

### Licence

Simple Slack Deleter is free software available under the MIT licence.  See
LICENCE for full details.

### Contributing

You can send pull requests against `simple_deleter.py` at
[Gitlab](https://gitlab.com/puzzlement/simple-slack-deleter).

Only fixes for outright bugs will be accepted. This is intended to be a very
simple to install and use demo-like script. If you want to do more complex
operations such as configurable deletion periods, command line arguments, etc,
you most likely want to permanently fork or start a separate project built
directly on top of [slack_cleaner2](https://github.com/sgratzl/slack_cleaner2).
